﻿using System;
using System.Collections;

namespace UF5M03ExercicisMartinezArmando
{
    public class Solicitud
    {
        public string NombreCliente { get; set; }
        public string TipoSolicitud { get; set; }
        public Solicitud(string ncliente, string tsolicitud)
        {
            this.NombreCliente = ncliente;
            this.TipoSolicitud = tsolicitud;
        }
    }
    public class ColaCliente : Queue
    {
        Queue cola { get; set; }
        public ColaCliente()
        {
            this.cola = new Queue();
        }

        public void AgregarSolicitud(Solicitud newSolicitud)
        {
            this.cola.Enqueue(newSolicitud);
        }
        public void MostrarSiguienteSolicitud()
        {
            Solicitud s = (Solicitud)this.cola.Peek();
            Console.WriteLine($"Cliente: {s.NombreCliente} - Tipo: {s.TipoSolicitud}");
        }
        public void EliminarSolicitudAtendida()
        {
            this.cola.Dequeue();
        }
        public void SolicitudesPendientes()
        {
            foreach (Solicitud s in this.cola)
            {
                Console.WriteLine($"Cliente: {s.NombreCliente} - Tipo: {s.TipoSolicitud}");
            }
        }
    }
}