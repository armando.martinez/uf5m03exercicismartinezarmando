﻿using System;

namespace UF5M03ExercicisMartinezArmando
{
    class Program
    {
        static void Main()
        {
            int option;
            do
            {
                option = Menu(new string[] { "\n1. Sistema de atención al cliente de un banco (FIFO)",
                                "2. Sistema de reparto de Just Feed (FIFO)",
                                "3. Super torneo de cartas Yu-Gi-Oh! (LIFO)",
                                "0. Salir"});
                Console.Clear();
                switch (option)
                {
                    case 1:
                        Color(ConsoleColor.Blue);
                        Console.WriteLine("\nSistema de atención al cliente de un banco\n");

                        ColaCliente cliente = new ColaCliente();
                        do
                        {
                            Console.Write("Nombre del cliente: ");
                            string nombre = Console.ReadLine();
                            Console.Write("Tipo de solicitud: ");
                            string tipo = Console.ReadLine();
                            cliente.AgregarSolicitud(new Solicitud(nombre, tipo));

                            Console.WriteLine($"Se ha atendido a la siguiente solicitud? [S] [N]");
                            cliente.MostrarSiguienteSolicitud();
                            if (Console.ReadLine().ToUpper() == "S") cliente.EliminarSolicitudAtendida();

                            cliente.SolicitudesPendientes();

                            Console.WriteLine("¿Terminar? [S] [N]");
                        } while (Console.ReadLine().ToUpper() != "S");
                        Pause();
                        break;
                    
                    
                    case 2:
                        Color(ConsoleColor.Cyan);
                        Console.WriteLine("\nSistema de reparto de Just Feed\n");

                        ColaReparto cola = new ColaReparto();
                        int suboption;
                        do
                        {
                            suboption = Menu(new string[] { "\n1. Agregar pedido", "2. Mostrar próximo pedido", "3. Eliminar próximo pedido", "0. Salir" });
                            switch (suboption)
                            {
                                case 1:
                                    Console.Write("Nombre");
                                    string nombre = Console.ReadLine();
                                    Console.Write("Dirección");
                                    string direccion = Console.ReadLine();
                                    Console.WriteLine("Platos: (separados por comas)");
                                    string[] platos = Console.ReadLine().Split(",");

                                    cola.AgregarPedido(nombre, direccion, platos);
                                    break;
                                case 2:
                                    cola.MostrarProximoPedido();
                                    break;
                                case 3:
                                    cola.EliminarProximoPedido();
                                    break;
                                case 0:
                                    break;
                            }
                        } while (suboption != 0);

                        Pause();
                        break;


                    case 3:
                        Color(ConsoleColor.Green);
                        Console.WriteLine("\nSuper torneo de cartas Yu-Gi-Oh!\n");
                        MazoCartas mazo = new MazoCartas();
                        ManoJugador mano = new ManoJugador();
                        // AÑADIR CARTAS
                        mazo.AgregarCarta(new Carta("Tamagotchi", "Digital", 34, "*se muere*"));
                        mazo.AgregarCarta(new Carta("Exodia", "Overpowered", 35, "Gana el juego"));
                        mazo.AgregarCarta(new Carta("Adrian", "Alumno", 20, "Tiene novia"));
                        mazo.AgregarCarta(new Carta("Paloma que vi una vez en una plaza", "Amiga", 6, "Picoteo"));
                        mazo.AgregarCarta(new Carta("Perro", "Perro", 7, ":D"));
                        mazo.AgregarCarta(new Carta("Tortilla", "Comida", 7, "Misterio concebollino"));
                        mazo.AgregarCarta(new Carta("Taco", "Comida", 2, "Delicioso taco al pastor"));
                        mazo.AgregarCarta(new Carta("Agumon", "Digital", 13, "Llama bebé"));
                        mazo.AgregarCarta(new Carta("Air Bud", "Perro", 24, "Le sabe a todos los deportes"));

                        Color();
                        mano.Mano.Add(mazo.RobarCarta());
                        do
                        {
                            Console.WriteLine("\nQue quieres hacer?");
                            suboption = Menu(new string[] { "1. Robar carta", "2. Jugar carta",
                                                            "3. Descartar carta", "0. Nada..."});
                            int selectCarta;
                            switch (suboption)
                            {
                                case 1:
                                    mano.Mano.Add(mazo.RobarCarta());
                                    break;
                                case 2:

                                    Console.WriteLine("Que carta quieres jugar?");
                                    selectCarta = MenuYugi(mano.ListaCartas());
                                    mano.JugarCarta(mano.Mano[selectCarta]);
                                    break;
                                case 3:
                                    Console.WriteLine("Que carta quieres jugar?");
                                    selectCarta = MenuYugi(mano.ListaCartas());
                                    mano.DescartarCarta(mano.Mano[selectCarta]);
                                    break;
                                case 0:
                                    Console.WriteLine("Huh, ok.");
                                    break;
                            }

                        } while (!mano.EstaVacia());


                        Pause();
                        break;
                    case 0:
                        Console.WriteLine("Programa finalizado...");
                        break;
                }
                Console.Clear();
            } while (option != 0);
        }
        // Mostra un menú en base a un array d'opcions
        static int Menu(string[] options)
        {
            Color(ConsoleColor.DarkGray);
            foreach (string option in options) { Console.WriteLine(option); }
            Color();
            int selectedOption = ParseIntInRange("\nSELECCIONA UNA OPCIÓN: ", 0, options.Length);
            return selectedOption;
        }

        static int MenuYugi(string[] options)
        {
            Color(ConsoleColor.DarkGray);
            foreach (string option in options) { Console.WriteLine(option); }
            Color();
            int selectedOption = ParseInt("\nSELECCIONA UNA OPCIÓN: ");
            return selectedOption;
        }
        // Demana una dada que obligatoriament ha d'estar dins d'un rang. Repeteix fins que es dona una dada adequada i la retorna.
        static int ParseInt(string prompt)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("¡El formato es incorrecto!"); }
            } while (!isParsed);
            return parsedNum;
        }
        static int ParseIntInRange(string prompt, int min, int max)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("¡El formato es incorrecto!"); }
                else if (parsedNum > max || parsedNum < min) { Console.WriteLine($"¡El numero debe estar entre {min} y {max}!"); }
            } while (!isParsed || parsedNum > max || parsedNum < min);
            return parsedNum;
        }
        // Crea una pause en el código
        static void Pause(string prompt = "\n[PRESS ENTER TO CONTINUE]\n")
        {
            Color(ConsoleColor.Black, ConsoleColor.Gray);
            Console.Write(prompt);
            Color();
            Console.ReadLine();
        }
        // Cambia el color de la consola
        static void Color(ConsoleColor fg = ConsoleColor.White, ConsoleColor bg = ConsoleColor.Black)
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
        }


    }
}
