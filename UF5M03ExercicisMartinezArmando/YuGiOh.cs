﻿using System;
using System.Collections.Generic;

namespace UF5M03ExercicisMartinezArmando
{
    class Carta
    {
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Nivel { get; set; }
        public string Efecto { get; set; }
        public Carta(string nombre, string tipo, int nivel, string efecto)
        {
            this.Nombre = nombre;
            this.Tipo = tipo;
            this.Nivel = nivel;
            this.Efecto = efecto;
        }
        override public string ToString()
        {
            return $"{this.Nombre}.{this.Tipo}.{this.Nivel}.{this.Efecto}";
        }
    }

    class MazoCartas : Stack<Carta>
    {
        Stack<Carta> Mazo { get; set; }
        public MazoCartas()
        {
            this.Mazo = new Stack<Carta>();
        }

        public void AgregarCarta(Carta carta)
        {
            this.Mazo.Push(carta);
        }
        public Carta RobarCarta()
        {
            if (EstaVacio()) return default(Carta);
            Carta c = this.Mazo.Peek();
            Console.WriteLine($"Robas {c.Nombre} ({c.Tipo}) - Nivel: {c.Nivel}\n{c.Efecto}");
            return this.Mazo.Pop();
        }
        public bool EstaVacio()
        {
            return this.Mazo.Count == 0;
        }
    }

    class ManoJugador
    {
        public List<Carta> Mano { get; set; }
        public ManoJugador()
        {
            this.Mano = new List<Carta>();
        }
        private int CartaInMano(Carta carta)
        {
            if (carta != null)
            {
                for (int i = 0; i < this.Mano.Count; i++)
                {
                    if (this.Mano[i] != null)
                    {
                        if (this.Mano[i].ToString() == carta.ToString()) return i;
                    }
                }
            }
            Console.WriteLine("¡Esta carta no está en la mano!");
            return -1;
        }
        public void JugarCarta(Carta carta)
        {
            if (this.CartaInMano(carta) != -1) 
            {
                Console.WriteLine($"\nJuegas {carta.Nombre} ({carta.Tipo}): {carta.Efecto}");
                DescartarCarta(carta);
            }
        }
        public void DescartarCarta(Carta carta)
        {
            int i = this.CartaInMano(carta);
            if (i != -1)
            {
                Console.WriteLine($"\nDescartas {carta.Nombre}");
                this.Mano[i] = null;
            }
        }
        public bool EstaVacia()
        {
            foreach(Carta c in this.Mano)
            {
                if (c != null) return false;
            }
            return true;
        }

        public String[] ListaCartas()
        {
            int c = 0;
            for (int i = 0; i < this.Mano.Count; i++)
            {
                if (this.Mano[i] != null) c++;
            }
            String[] lista = new string[c];
            int j = 0;
            for (int i = 0; i < this.Mano.Count && j < c; i++)
            {
                if (this.Mano[i] != null)
                {
                    lista[j] = $"{i}. {this.Mano[i].Nombre} ({this.Mano[i].Tipo})";
                    j++;
                }
            }
            return lista;
        }
    }
}