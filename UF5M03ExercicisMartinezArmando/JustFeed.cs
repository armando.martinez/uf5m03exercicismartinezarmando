﻿using System;
using System.Collections;

namespace UF5M03ExercicisMartinezArmando
{
    public class Pedido
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string[] Platos { get; set; }
        public Pedido(string nombre, string direccion, string[] platos)
        {
            this.Nombre = nombre;
            this.Direccion = direccion;
            this.Platos = platos;
        }
    }
    public class ColaReparto : Queue
    {
        private Queue cola { get; set; }
        public ColaReparto()
        {
            this.cola = new Queue();
        }

        public void AgregarPedido(string nombre, string direccion, string[] platos)
        {
            this.cola.Enqueue(new Pedido(nombre, direccion, platos));
        }
        public void MostrarProximoPedido()
        {
            Pedido p = (Pedido)this.cola.Peek();
            Console.WriteLine($"Nombre del cliente: {p.Nombre}\nDirección de entrega: {p.Direccion}\nPlatos:\n");
            foreach (string plato in p.Platos) Console.WriteLine($"\t{plato}");
        }
        public void EliminarProximoPedido()
        {
            this.cola.Dequeue();
        }
    }
}